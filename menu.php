<?php require('file.php'); ?>
<!DOCTYPE html>
<html>
<style>
html, body
{
    background-color: #C0C0C0;
    padding-top: 20px;
     background-attachment: fixed;
        background-size: 100%;
}
#log
{
        margin-left:30%;
        font-family:arial;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
h1{
      background-color: black;
      padding-left: 40%;
      color:white;
    }
    .button{
      background-color:#C0C0C0;
      color:white;
      border:0;
      font-family: arial;
    }
    .button:hover{
      background-color: black;
      color:white;
    }

.topnav
{
    position: fixed;
    z-index: 999;
    left: 0;
    right: 0; 
    top: 0;
    height: 4em;
    padding: 0 40px;
    background-color: #C0C0C0;
}
ul.navbar-left li a.active {
    color: white;
    text-decoration: none;
    font-family: 'Poppins', sans-serif;
}
div.topnav ul li {
    display: inline-block;
    margin: 20px;
}
.dropbtn
{
    display: inline-block;
  color: white;
  text-align: center;
  padding: 0px 0px;
  text-decoration: none;
}
.dropdown:hover, .dropbtn
{
    
}
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1;}

.dropdown:hover .dropdown-content {
  display: block;
}
#logoo
{
  font-family: 'Poppins', sans-serif;
  color:white;

}
.pesan table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  border-style:solid;
  border-color:white;  
}
.pesan th, td, h2 {
  padding: 5px;
  color:lightgrey;
}
.pesan th {
  text-align: left;
  margin-top: 100px;
}
.bawah
{
  padding-top: 10px;
  background-color: #1E1D22;
  width:100%;
  height:75px;
  color:lightgrey;
  text-align: center;
}
.pesan
{
  background-color:black;
  padding-top: 50px;;
}
.pesan h2
{
  text-align: center;
}
</style>
<head>
  <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="main.js"></script>
    <title>KUMPUL YUK!</title>
    <link rel="icon" href="icon.jpg" type="image/jpg" sizes="20x20">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</head>
<body data-spy="scroll"data-target="#beranda" data-offset="50">
  
  <nav>
    <div class="topnav" id="navbar"> 
      <!-- Brand/logo -->
        <img src="logoku.png" width="65px">
      <ul class="navbar-left" style="display: inline-block">
        <li>
          <a href="indexx.php" class="active">beranda</a> 
        </li>
        <li>
          <a href="indexx.php#ttg" class="active">tentang</a> 
        </li>
                <li class="dropdown">
                    <div class="dropdown">
                    <a href="#tempat" class="dropbtn">Lokasi</a>
                    <div class="dropdown-content">
                        <a href="surabaya.php">Surabaya</a>
                        <a href="yogyakarta.php">Yogyakarta</a>
                </div>
                </li>
                <li>
                    <a href="keranjang.php" class="active">Keranjang</a>
                </li>
                <li>
                    <?php 
                    if(empty($_SESSION['username'])){echo "<a href=\"hlmlogin.php\" class=\"active\">Login</a>";} 
                    else {echo $_SESSION['username'];}
                    ?>
                </li>
      </ul>
    </div>
  </nav>
               

<div class="pesan">
  <h2><b>DAFTAR MENU</b></h2>
  <form method="post">
<table style="width:100%">
  <tr>
    <th><b>Menu</b></th> 
    <th><b>Harga</b></th>
  </tr>
  <tr>
    <td>
      <input type="radio" name="pilihan" value="paket 1">
       <label>&nbsp;PAKET 1</label>
        <p>- Gurame keroyokan 1 porsi</p>
          <p>- nasi putih 2 porsi</p>
          <p>- air mineral 600ml 2 botol</p>
    </td>
    <td>
      <p>71k (untuk 2 orang)</p>
    </td>
  <tr>
    <td>
      <input type="radio" name="pilihan" value="paket 2">
       <label>&nbsp;PAKET 2</label>
      <p>- kepiting keroyokan 1 porsi</p>
      <p>- nasi putih 2 porsi</p>
      <P>- Air mineral 600ml 2 botol</P>
    </td>
    <td>
      <p>81,9k (untuk 2 orang)</p>
    </td>
  </tr>
    <tr>
    <td>
      <input type="radio" name="pilihan" value="paket 3">
       <label>&nbsp;PAKET 3</label>
      <p>- kepiting keroyokan 1 porsi</p>
      <p>- Tumisan sayur 2 porsi</p>
      <p>- nasi putih 3 porsi</p>
      <P>- Air mineral 600ml 3 botol</P>
    </td>
    <td>
      <p>91, 2k(untuk 2 orang)</p>
    </td>
    </tr>
</table>
<button name="submitOrder" onclick="alert('pilihan sudah dimasukkan');">Submit</button></form>
</div>
 <footer>
      <div class="bawah">
        <p>&copy 2019 by Babaz</p>
      </div>
    </footer>