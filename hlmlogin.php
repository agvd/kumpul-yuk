<?php include("file.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="main.js"></script>
    <title>KUMPUL YUK!</title>
    <link rel="icon" href="icon.jpg" type="image/jpg" sizes="20x20">
</head>
<style>
    body{
        background-color:#C0C0C0;
        background-attachment: fixed;
        background-size: 100%;
    }
    #log{
        margin-left:30%;
        font-family:arial;
    }
    h1{
      background-color: black;
      padding-left: 40%;
      color:white;
    }
    .button{
      background-color:#C0C0C0;
      color:white;
      border:0;
      font-family: arial;
    }
    .button:hover{
      background-color: black;
      color:white;
    }

</style>
<body>
    <div class="container" style="width:100%;height:100%;">
            <div class="w3-display-middle"  style="width:100%;">
                <div id="log" style="width:43%;">
                        <h1>Log In</h1>

                    <form method="post" autocomplete="off" class="w3-container w3-white">
                        <br><label>ID</label>
                        <input type="text" name="usernamelog" class="w3-input">
                        <br><label>Kata Sandi</label>
                        <input type="password" name="passwordlog" class="w3-input">
                        <br><a class="w3-text-blue" style="text-decoration: none;" href="signup.php">Sign Up</a><br><br>
                        <button class="button" name="login">Log In</button><br><br>
                    </form>
                </div>

                
            </div>
    </div>
</body>
</html>