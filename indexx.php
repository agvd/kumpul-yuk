<?php require('file.php');?>
<!DOCTYPE html>
<html>
<style>
html, body
{
    background-color: #C0C0C0;
    padding-top: 20px;
     background-attachment: fixed;
        background-size: 100%;
}
#log
{
        margin-left:30%;
        font-family:arial;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
h1{
      background-color: black;
      padding-left: 40%;
      color:white;
    }
    .button{
      background-color:#C0C0C0;
      color:white;
      border:0;
      font-family: arial;
    }
    .button:hover{
      background-color: black;
      color:white;
    }

.topnav
{
    position: fixed;
    z-index: 999;
    left: 0;
    right: 0; 
    top: 0;
    height: 4em;
    padding: 0 40px;
    background-color: #C0C0C0;
}
ul.navbar-left li a.active {
    color: white;
    text-decoration: none;
    font-family: 'Poppins', sans-serif;
}
div.topnav ul li {
    display: inline-block;
    margin: 20px;
}
.dropbtn
{
    display: inline-block;
  color: white;
  text-align: center;
  padding: 0px 0px;
  text-decoration: none;
}
.dropdown:hover, .dropbtn
{
    
}
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1;}

.dropdown:hover .dropdown-content {
  display: block;
}
.header1
{
	width: 100%;
	overflow: hidden;
	background-color: #C0C0C0;
}
.header1 img
{
	background-image: 100%;
	width: 100%;
}
.first
{
	padding-bottom: 0;
}
.bawah
{
	padding-top: 10px;
	background-color: #1E1D22;
	width:100%;
	height:75px;
	color:lightgrey;
	text-align: center;
}
.tabel a:hover 
{
	opacity: 0.5;
}
.about
{
	background-color: white;
	color: black;
	padding-top: 50px;
	padding-bottom: 200px;
}
.about img
{
	float: left;
	width: 300px;
	height: 250px;
}
.about p
{
	padding-top: 100px;
	text-align: left;
	margin-right: 20px;
	margin-left: 50px;
}
#log
{
    background-color: black;
}
#logoo
{
	font-family: 'Poppins', sans-serif;
	color:white;

}
</style>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="main.js"></script>
    <title>KUMPUL YUK!</title>
    <link rel="icon" href="icon.jpg" type="image/jpg" sizes="20x20">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</head>
<body data-spy="scroll"data-target="#beranda" data-offset="50">
	<nav>
		<div class="topnav" id="navbar"> 
			<!-- Brand/logo -->
  			<img src="logoku.png" width="65px">
			<ul class="navbar-left" style="display: inline-block">
				<li>
					<a href="index.php" class="active">beranda</a> 
				</li>
				<li>
					<a href="#ttg" class="active">tentang</a> 
				</li>
                <li class="dropdown">
                    <div class="dropdown">
                    <a href="#tempat" class="dropbtn">Lokasi</a>
                    <div class="dropdown-content">
                        <a href="surabaya.php">Surabaya</a>
                        <a href="yogyakarta.php">Yogyakarta</a>
                </div>
                </li>
                <li>
                    <a href="keranjang.php" class="active">Keranjang</a>
                </li>
                <li>
                    <?php 
                    if(empty($_SESSION['username'])){echo "<a href=\"hlmlogin.php\" class=\"active\">Login</a>";} 
                    else {echo $_SESSION['username'];}
                    ?>
                </li>
			</ul>
		</div>
	</nav>
	<div class="header1"> 
    <section id="beranda">
    	<div class="first">
      	<img src="cafe2.jpg" width="100%">
    </div>
    </section>
    <div class="tabel">
    	<section id="tempat">
    		<table style="width: 100%">
    			<th>
					<img src="caffee.jpg" width="300px" >
    			</th>
    			<th>
					<img src="restaurantt.jpg" width="300px" >
    			</th>
    		</table>
    		</div>
    	</section>
    </div>
    <div class="about">
    	<section id="ttg">
    		
    		<h2><img src="logoku.png" style="float: left;"></h2>
    		<p>
    			KUMPUL YUK! adalah web yang memudahkan semua orang untuk mencari referensi dan informasi mengenai 
    			restoran dan cafe yang terekomendasi, terutama bagi para kaula muda yang sering mengadakan kumpul 
    			bersama akan dimudahkan untuk mencari nama tempat, alamat menuju lokasi, serta budget makanan atau
    			minuman yang ditawarkan dari tempat tersebut. Dengan bantuan kontak telepon, pengguna website akan 
    			merasa teringankan.
    		</p>

    	</section>
    </div>
    
    </div>
    <footer>
    	<div class="bawah">
    		<p>&copy 2019 by Babaz</p>
    	</div>
    </footer>


</body>
</html>